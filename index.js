"use strict";
var http = require('http');
var fs = require('fs');
var rows = fs.readFileSync('./Hospital.csv', 'utf8').split("\r\n");
var hospitals = [];
for (var x = 1; x < rows.length; x++) {
    var row = rows[x].split(',');
    hospitals.push({
        OrganisationID: row[0],
        OrganisationCode: row[1],
        OrganisationName: row[5],
        Latitude: parseFloat(row[7]),
        Longitude: parseFloat(row[8]),
    });
}
var getNearest = function (lat, lon) {
    var distance = function (lat1, lon1, lat2, lon2) {
        var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
        var y = (lat2 - lat1);
        return Math.sqrt(x * x + y * y) * 6371000;
    };
    var compare = function (a, b) {
        var rad = function (deg) {
            return deg * Math.PI / 180;
        };
        var distanceA = distance(rad(lat), rad(lon), rad(a.Latitude), rad(a.Longitude));
        var distanceB = distance(rad(lat), rad(lon), rad(b.Latitude), rad(b.Longitude));
        return distanceA < distanceB ? -1 : distanceA > distanceB ? 1 : 0;
    };
    return hospitals.sort(compare).slice(0, 5);
};
http.createServer(function (request, response) {
    var done = function (status, message) {
        response.writeHead(status, {'Content-Type': 'application/json'});
        response.end(JSON.stringify(message));
    };
    try {
        var splitRoute = request.url.split('?');
        var url = splitRoute[0];
        if (request.method !== 'GET' || !(url === '/nearest' || url === '/nearest/')) {
            return done(404, {error: 'not found'});
        }
        if (splitRoute.length !== 2) {
            return done(422, {error: 'you must provide lat/lon parmaters'});
        }
        var splitParams = splitRoute[1].split('&');
        var params = {};
        for (var x = 0; x < splitParams.length; x++) {
            var item = splitParams[x].split('=');
            if (item.length === 2 && (item[0] === 'lat' || item[0] === 'lon')) {
                params[item[0]] = parseFloat(item[1])
            }
        }
        if (params.lat === undefined || Number.isNaN(params.lat) || params.lon === undefined || Number.isNaN(params.lon)) {
            return done(422, {error: 'you must provide valid lat/lon parmaters'});
        }
        return done(200, getNearest(params.lat, params.lon));
    } catch (e) {
        console.error(e);
        return done(500, {error: 'A server error occurred'});
    }
}).listen(3000, function () {
    console.log('Listening on port 3000');
});
