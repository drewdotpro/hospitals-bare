# NodeJS Code Challenge (Bare NodeJS)

## Repository Structure

`index.js` is the application entry point

## Setup

No setup is required. This NodeJS application runs without any additional dependencies. 

Tested on all major LTS versions of NodeJS from 0.12+

## Usage
```
node index.js

```
From here you can cURL or request the data in your browser, for example, requesting:
```
http://localhost:3000/nearest?lat=53.85313416&lon=-0.411472321
```
Will give the return data:
```
[{"organisationID":"1421","organisationCode":"RV9HE","organisationName":"East Riding Community Hospital","Latitude":"53.85313416","Longitude":"-0.411472321"},{"organisationID":"42260","organisationCode":"RV941","organisationName":"Hawthorne Court - Inpatient Unit","Latitude":"53.84577942","Longitude":"-0.434020907"},{"organisationID":"2027176","organisationCode":"RV9FH","organisationName":"Humber Centre for Forensic Psychiatry","Latitude":"53.7702446","Longitude":"-0.44977659"},{"organisationID":"42661","organisationCode":"RWA16","organisationName":"Castle Hill Hospital","Latitude":"53.7767334","Longitude":"-0.444969475"},{"organisationID":"70616","organisationCode":"RV942","organisationName":"Mill View Court - Inpatient Unit","Latitude":"53.7767334","Longitude":"-0.444969475"}]
```

Malformed requests will receive a 422 response and data like:
```
{"error":"you must provide valid lat/lon parmaters"}
```

Server errors will respond with 500 response and data like:
```
{"error":"A server error occured"}
```
